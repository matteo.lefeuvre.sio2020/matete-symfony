<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/admin' => [[['_route' => 'admin', '_controller' => 'App\\Controller\\Admin\\DashboardController::index'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'matete', '_controller' => 'App\\Controller\\MateteController::index'], null, null, null, false, false, null]],
        '/matete' => [[['_route' => 'home', '_controller' => 'App\\Controller\\MateteController::home'], null, null, null, false, false, null]],
        '/annonces' => [[['_route' => 'annonces', '_controller' => 'App\\Controller\\MateteController::annonces'], null, null, null, false, false, null]],
        '/annonces/new' => [[['_route' => 'new', '_controller' => 'App\\Controller\\MateteController::creer'], null, null, null, false, false, null]],
        '/categorie/new' => [[['_route' => 'ajout_categorie', '_controller' => 'App\\Controller\\MateteController::newCategorie'], null, null, null, false, false, null]],
        '/mesannonces' => [[['_route' => 'mesannonces', '_controller' => 'App\\Controller\\MateteController::mesannonces'], null, null, null, false, false, null]],
        '/amongus' => [[['_route' => 'amongus', '_controller' => 'App\\Controller\\MateteController::amongus'], null, null, null, false, false, null]],
        '/panier' => [[['_route' => 'panier', '_controller' => 'App\\Controller\\MateteController::panier'], null, null, null, false, false, null]],
        '/map' => [[['_route' => 'map', '_controller' => 'App\\Controller\\MateteController::map'], null, null, null, false, false, null]],
        '/mesemplacement' => [[['_route' => 'mesemplacement', '_controller' => 'App\\Controller\\MateteController::mesemplacement'], null, null, null, false, false, null]],
        '/emplacement/new' => [[['_route' => 'ajout_emplacement', '_controller' => 'App\\Controller\\MateteController::newEmplacement'], null, null, null, false, false, null]],
        '/security' => [[['_route' => 'security', '_controller' => 'App\\Controller\\SecurityController::index'], null, null, null, false, false, null]],
        '/inscription' => [[['_route' => 'security_registration', '_controller' => 'App\\Controller\\SecurityController::registration'], null, null, null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/delete_user' => [[['_route' => 'user_delete', '_controller' => 'App\\Controller\\SecurityController::deleteUser'], null, null, null, false, false, null]],
        '/confirm' => [[['_route' => 'confirm', '_controller' => 'App\\Controller\\SecurityController::confirm'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|wdt/([^/]++)(*:24)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:69)'
                            .'|router(*:82)'
                            .'|exception(?'
                                .'|(*:101)'
                                .'|\\.css(*:114)'
                            .')'
                        .')'
                        .'|(*:124)'
                    .')'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:159)'
                .')'
                .'|/a(?'
                    .'|nnonce/([^/]++)(?'
                        .'|(*:191)'
                        .'|/edit(*:204)'
                    .')'
                    .'|joutpanier/([^/]++)(*:232)'
                .')'
                .'|/retirerpanier/([^/]++)(*:264)'
                .'|/matete/([^/]++)/delete(*:295)'
                .'|/delete/([^/]++)(*:319)'
                .'|/profil/([^/]++)(?'
                    .'|(*:346)'
                    .'|/edit(*:359)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        24 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        69 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        82 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        101 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        114 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        124 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        159 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        191 => [[['_route' => 'annonce_show', '_controller' => 'App\\Controller\\MateteController::show'], ['id'], null, null, false, true, null]],
        204 => [[['_route' => 'annonce_edit', '_controller' => 'App\\Controller\\MateteController::creer'], ['id'], null, null, false, false, null]],
        232 => [[['_route' => 'ajoutpanier', '_controller' => 'App\\Controller\\MateteController::ajoutpanier'], ['id'], null, null, false, true, null]],
        264 => [[['_route' => 'retirerpanier', '_controller' => 'App\\Controller\\MateteController::retirerpanier'], ['id'], null, null, false, true, null]],
        295 => [[['_route' => 'matete_delete', '_controller' => 'App\\Controller\\MateteController::delete'], ['id'], null, null, false, false, null]],
        319 => [[['_route' => 'annonce_delete', '_controller' => 'App\\Controller\\MateteController::deleteEmplacement'], ['id'], null, null, false, true, null]],
        346 => [[['_route' => 'profil', '_controller' => 'App\\Controller\\SecurityController::profil'], ['id'], null, null, false, true, null]],
        359 => [
            [['_route' => 'profil_edit', '_controller' => 'App\\Controller\\SecurityController::editProfil'], ['id'], null, null, false, false, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
