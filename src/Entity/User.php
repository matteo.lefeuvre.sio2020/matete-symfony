<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity; 

/** 
 * @ORM\Entity(repositoryClass=UserRepository::class) 
 * @UniqueEntity( 
 *     fields={"email"}, 
 *     message="Ce mail ou ce nom d'utilisateur existe déjà." 
 * ) 
 */ 
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Email(message = "The email '{{ value }}' is not a valid email.")
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /** 
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\Length( 
     *      min = 6, 
     *      max = 50, 
     *      minMessage = "Your first name must be at least {{ limit }} characters long", 
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters")
     */
    private $password;

    /** 
     * @Assert\EqualTo(propertyPath="password",message = "La confirmation ne correspond pas") 
     */ 
    public $confirm_password;

    /**
     * @ORM\OneToMany(targetEntity=Annonce::class, mappedBy="LeProducteur")
     */
    public $lesAnnonces;

    public function __construct()
    {
        $this->lesAnnonces = new ArrayCollection();
    } 


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function addRole($role){
        array_push($this->roles, $role);
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function isAdmin(): bool
    {
        return in_array("ROLE_ADMIN", $this->roles);
    }

    /**
     * @return Collection|Annonce[]
     */
    public function getLesAnnonces(): Collection
    {
        return $this->lesAnnonces;
    }

    public function addLesAnnonce(Annonce $lesAnnonce): self
    {
        if (!$this->lesAnnonces->contains($lesAnnonce)) {
            $this->lesAnnonces[] = $lesAnnonce;
            $lesAnnonce->setLeProducteur($this);
        }

        return $this;
    }

    public function removeLesAnnonce(Annonce $lesAnnonce): self
    {
        if ($this->lesAnnonces->removeElement($lesAnnonce)) {
            // set the owning side to null (unless already changed)
            if ($lesAnnonce->getLeProducteur() === $this) {
                $lesAnnonce->setLeProducteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Emplacement[]
     */
    public function getLesEmplacements(): Collection
    {
        return $this->lesEmplacements;
    }

    public function addLesEmplacement(Emplacement $lesEmplacement): self
    {
        if (!$this->lesEmplacements->contains($lesEmplacement)) {
            $this->lesEmplacements[] = $lesEmplacement;
            $lesEmplacement->setLeProducteur($this);
        }

        return $this;
    }

    public function removeLesEmplacement(Emplacement $lesEmplacement): self
    {
        if ($this->lesEmplacements->removeElement($lesEmplacement)) {
            // set the owning side to null (unless already changed)
            if ($lesEmplacement->getLeProducteur() === $this) {
                $lesEmplacement->setLeProducteur(null);
            }
        }

        return $this;
    }

    public function __toString() {
        return $this->nom;
    }

}