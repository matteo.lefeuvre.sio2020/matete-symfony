<?php

namespace ContainerN6x30bD;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder503af = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerfd220 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties24bf4 = [
        
    ];

    public function getConnection()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'getConnection', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'getMetadataFactory', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'getExpressionBuilder', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'beginTransaction', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'getCache', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->getCache();
    }

    public function transactional($func)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'transactional', array('func' => $func), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'wrapInTransaction', array('func' => $func), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'commit', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->commit();
    }

    public function rollback()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'rollback', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'getClassMetadata', array('className' => $className), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'createQuery', array('dql' => $dql), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'createNamedQuery', array('name' => $name), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'createQueryBuilder', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'flush', array('entity' => $entity), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'clear', array('entityName' => $entityName), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->clear($entityName);
    }

    public function close()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'close', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->close();
    }

    public function persist($entity)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'persist', array('entity' => $entity), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'remove', array('entity' => $entity), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'refresh', array('entity' => $entity), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'detach', array('entity' => $entity), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'merge', array('entity' => $entity), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'getRepository', array('entityName' => $entityName), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'contains', array('entity' => $entity), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'getEventManager', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'getConfiguration', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'isOpen', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'getUnitOfWork', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'getProxyFactory', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'initializeObject', array('obj' => $obj), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'getFilters', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'isFiltersStateClean', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'hasFilters', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return $this->valueHolder503af->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerfd220 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder503af) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder503af = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder503af->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, '__get', ['name' => $name], $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        if (isset(self::$publicProperties24bf4[$name])) {
            return $this->valueHolder503af->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder503af;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder503af;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder503af;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder503af;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, '__isset', array('name' => $name), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder503af;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder503af;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, '__unset', array('name' => $name), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder503af;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder503af;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, '__clone', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        $this->valueHolder503af = clone $this->valueHolder503af;
    }

    public function __sleep()
    {
        $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, '__sleep', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;

        return array('valueHolder503af');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerfd220 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerfd220;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerfd220 && ($this->initializerfd220->__invoke($valueHolder503af, $this, 'initializeProxy', array(), $this->initializerfd220) || 1) && $this->valueHolder503af = $valueHolder503af;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder503af;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder503af;
    }


}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
