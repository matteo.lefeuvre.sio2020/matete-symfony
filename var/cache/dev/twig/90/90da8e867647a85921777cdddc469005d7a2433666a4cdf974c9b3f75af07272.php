<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* matete/creer.html.twig */
class __TwigTemplate_4cf0252bb2284bad400cdfdfb903f78165ce21b01587deb36ed365861b6e70fc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/creer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/creer.html.twig"));

        // line 5
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["formAnnonce"]) || array_key_exists("formAnnonce", $context) ? $context["formAnnonce"] : (function () { throw new RuntimeError('Variable "formAnnonce" does not exist.', 5, $this->source); })()), [0 => "bootstrap_4_layout.html.twig"], true);
        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "matete/creer.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Creation d'annonce";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "


<br>


";
        // line 14
        if (((isset($context["editMode"]) || array_key_exists("editMode", $context) ? $context["editMode"] : (function () { throw new RuntimeError('Variable "editMode" does not exist.', 14, $this->source); })()) && (0 !== twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 14, $this->source); })()), "user", [], "any", false, false, false, 14), "id", [], "any", false, false, false, 14), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["annonce"]) || array_key_exists("annonce", $context) ? $context["annonce"] : (function () { throw new RuntimeError('Variable "annonce" does not exist.', 14, $this->source); })()), "leProducteur", [], "any", false, false, false, 14), "id", [], "any", false, false, false, 14))))) {
            // line 15
            echo "
<h2>Vous ne pouvez pas éditer cette annonce  </h2>

";
        } else {
            // line 19
            echo "
<h1> Creation d'annonce </h1>

";
            // line 22
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formAnnonce"]) || array_key_exists("formAnnonce", $context) ? $context["formAnnonce"] : (function () { throw new RuntimeError('Variable "formAnnonce" does not exist.', 22, $this->source); })()), 'form_start');
            echo "

";
            // line 24
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formAnnonce"]) || array_key_exists("formAnnonce", $context) ? $context["formAnnonce"] : (function () { throw new RuntimeError('Variable "formAnnonce" does not exist.', 24, $this->source); })()), "titre", [], "any", false, false, false, 24), 'row', ["attr" => ["placeholder" => "Titre de l'annonce"]]);
            echo "
";
            // line 25
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formAnnonce"]) || array_key_exists("formAnnonce", $context) ? $context["formAnnonce"] : (function () { throw new RuntimeError('Variable "formAnnonce" does not exist.', 25, $this->source); })()), "creneauxDebut", [], "any", false, false, false, 25), 'row', ["attr" => ["placeholder" => "Créneaux de début"]]);
            echo "
";
            // line 26
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formAnnonce"]) || array_key_exists("formAnnonce", $context) ? $context["formAnnonce"] : (function () { throw new RuntimeError('Variable "formAnnonce" does not exist.', 26, $this->source); })()), "creneauxFin", [], "any", false, false, false, 26), 'row', ["attr" => ["placeholder" => "Créneaux de fin"]]);
            echo "
";
            // line 27
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formAnnonce"]) || array_key_exists("formAnnonce", $context) ? $context["formAnnonce"] : (function () { throw new RuntimeError('Variable "formAnnonce" does not exist.', 27, $this->source); })()), "libelleProduit", [], "any", false, false, false, 27), 'row', ["attr" => ["placeholder" => "Libelle du produit"]]);
            echo "
";
            // line 28
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formAnnonce"]) || array_key_exists("formAnnonce", $context) ? $context["formAnnonce"] : (function () { throw new RuntimeError('Variable "formAnnonce" does not exist.', 28, $this->source); })()), "prixUnitaire", [], "any", false, false, false, 28), 'row', ["attr" => ["placeholder" => "Prix unitaire du produit"]]);
            echo "
";
            // line 29
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formAnnonce"]) || array_key_exists("formAnnonce", $context) ? $context["formAnnonce"] : (function () { throw new RuntimeError('Variable "formAnnonce" does not exist.', 29, $this->source); })()), "qte", [], "any", false, false, false, 29), 'row', ["attr" => ["placeholder" => "Quantité"]]);
            echo "
";
            // line 30
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formAnnonce"]) || array_key_exists("formAnnonce", $context) ? $context["formAnnonce"] : (function () { throw new RuntimeError('Variable "formAnnonce" does not exist.', 30, $this->source); })()), "laCategorie", [], "any", false, false, false, 30), 'row', ["attr" => ["placeholder" => "Catégorie du produit"]]);
            echo "
";
            // line 31
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formAnnonce"]) || array_key_exists("formAnnonce", $context) ? $context["formAnnonce"] : (function () { throw new RuntimeError('Variable "formAnnonce" does not exist.', 31, $this->source); })()), "lEmplacement", [], "any", false, false, false, 31), 'row', ["attr" => ["placeholder" => "Lieu du producteur"]]);
            echo "
";
            // line 32
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formAnnonce"]) || array_key_exists("formAnnonce", $context) ? $context["formAnnonce"] : (function () { throw new RuntimeError('Variable "formAnnonce" does not exist.', 32, $this->source); })()), "Image", [], "any", false, false, false, 32), 'row', ["attr" => ["placeholder" => "Image"]]);
            echo "



<button type=\"submit\" class=\"btn btn-dark\"> 
    ";
            // line 37
            if ((isset($context["editMode"]) || array_key_exists("editMode", $context) ? $context["editMode"] : (function () { throw new RuntimeError('Variable "editMode" does not exist.', 37, $this->source); })())) {
                // line 38
                echo "        Enregistrer
    ";
            } else {
                // line 40
                echo "        Ajouter l'annonce
    ";
            }
            // line 42
            echo "</button>

";
            // line 44
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formAnnonce"]) || array_key_exists("formAnnonce", $context) ? $context["formAnnonce"] : (function () { throw new RuntimeError('Variable "formAnnonce" does not exist.', 44, $this->source); })()), 'form_end');
            echo "

";
        }
        // line 47
        echo "



";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "matete/creer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 47,  171 => 44,  167 => 42,  163 => 40,  159 => 38,  157 => 37,  149 => 32,  145 => 31,  141 => 30,  137 => 29,  133 => 28,  129 => 27,  125 => 26,  121 => 25,  117 => 24,  112 => 22,  107 => 19,  101 => 15,  99 => 14,  91 => 8,  81 => 7,  62 => 3,  51 => 1,  49 => 5,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Creation d'annonce{% endblock %}

{% form_theme formAnnonce 'bootstrap_4_layout.html.twig' %}

{% block body %}



<br>


{% if editMode and app.user.id != annonce.leProducteur.id %}

<h2>Vous ne pouvez pas éditer cette annonce  </h2>

{% else %}

<h1> Creation d'annonce </h1>

{{ form_start(formAnnonce) }}

{{ form_row(formAnnonce.titre, {'attr': {'placeholder': \"Titre de l'annonce\"}})}}
{{ form_row(formAnnonce.creneauxDebut, {'attr': {'placeholder': \"Créneaux de début\"}})}}
{{ form_row(formAnnonce.creneauxFin, {'attr': {'placeholder': \"Créneaux de fin\"}})}}
{{ form_row(formAnnonce.libelleProduit, {'attr': {'placeholder': \"Libelle du produit\"}})}}
{{ form_row(formAnnonce.prixUnitaire, {'attr': {'placeholder': \"Prix unitaire du produit\"}})}}
{{ form_row(formAnnonce.qte, {'attr': {'placeholder': \"Quantité\"}})}}
{{ form_row(formAnnonce.laCategorie, {'attr': {'placeholder': \"Catégorie du produit\"}})}}
{{ form_row(formAnnonce.lEmplacement, {'attr': {'placeholder': \"Lieu du producteur\"}})}}
{{ form_row(formAnnonce.Image, {'attr': {'placeholder': \"Image\"}})}}



<button type=\"submit\" class=\"btn btn-dark\"> 
    {% if editMode %}
        Enregistrer
    {% else %}
        Ajouter l'annonce
    {% endif %}
</button>

{{ form_end(formAnnonce) }}

{% endif %}




{% endblock %}", "matete/creer.html.twig", "C:\\Users\\Matteo\\Matete\\templates\\matete\\creer.html.twig");
    }
}
