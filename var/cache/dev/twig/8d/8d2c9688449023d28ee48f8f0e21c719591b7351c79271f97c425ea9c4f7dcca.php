<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* matete/mesannonces.html.twig */
class __TwigTemplate_223ba9e6e4daa17266dbd6731c2c2f879b9234f9e2783ec15d702d1d38d817bf extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/mesannonces.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/mesannonces.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "matete/mesannonces.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Liste de mes Annonces";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "

<br>
<div class=\"container\">
<div class=\"row\">

";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_reverse_filter($this->env, (isset($context["annonces"]) || array_key_exists("annonces", $context) ? $context["annonces"] : (function () { throw new RuntimeError('Variable "annonces" does not exist.', 12, $this->source); })())));
        foreach ($context['_seq'] as $context["_key"] => $context["annonce"]) {
            // line 13
            echo "
";
            // line 14
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["annonce"], "LeProducteur", [], "any", false, false, false, 14), "id", [], "any", false, false, false, 14)) {
            }
            // line 16
            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 16, $this->source); })()), "user", [], "any", false, false, false, 16), "id", [], "any", false, false, false, 16), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["annonce"], "LeProducteur", [], "any", false, false, false, 16), "id", [], "any", false, false, false, 16)))) {
                // line 17
                echo "
<br>
<annonce>

        <div class=\"col-md3 mt-3 card mb-3 Tout ";
                // line 21
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["annonce"], "laCategorie", [], "any", false, false, false, 21), "libelle", [], "any", false, false, false, 21), "html", null, true);
                echo "\" style=\"height:55rem\">
  <h3 class=\"card-header\">";
                // line 22
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "titre", [], "any", false, false, false, 22), "html", null, true);
                echo "</h3>
  <div class=\"card-body\">
    <h5 class=\"card-title\">Produit en vente:</h5>
    <h6 class=\"card-subtitle text-muted\">";
                // line 25
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "libelleProduit", [], "any", false, false, false, 25), "html", null, true);
                echo "</h6>
  </div>
<img src=\"https://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/73/736ada6647d9b5234d5e5e2662a3111bccf02f78_full.jpg\" eight=\"150px\" width=\"300\" alt=\"\">    <rect width=\"100%\" height=\"100%\" fill=\"#868e96\"></rect>
  </svg>
  <div class=\"card-body\">
    <p class=\"card-text\">Disponible du ";
                // line 30
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "creneauxDebut", [], "any", false, false, false, 30), "d/m H:i"), "html", null, true);
                echo " au ";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "creneauxFin", [], "any", false, false, false, 30), "d/m H:i"), "html", null, true);
                echo "</p>
  </div>
  <ul class=\"list-group list-group-flush\">
    <li class=\"list-group-item\">Le prix est ";
                // line 33
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "prixUnitaire", [], "any", false, false, false, 33), "html", null, true);
                echo "</li>
    <li class=\"list-group-item\">Quantité disponible : ";
                // line 34
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "qte", [], "any", false, false, false, 34), "html", null, true);
                echo "</li>
    <li class=\"list-group-item\" style=\"width:350px\">Catégorie : ";
                // line 35
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["annonce"], "laCategorie", [], "any", false, false, false, 35), "libelle", [], "any", false, false, false, 35), "html", null, true);
                echo "</li>
    
  </ul>
  <div class=\"card-body\">
    <a href=\"";
                // line 39
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annonce_show", ["id" => twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 39)]), "html", null, true);
                echo "\" class=\"btn btn-primary\"> Lire la suite</a>
  </div>
</div>

</annonce>


";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['annonce'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "
</div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "matete/mesannonces.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 48,  155 => 39,  148 => 35,  144 => 34,  140 => 33,  132 => 30,  124 => 25,  118 => 22,  114 => 21,  108 => 17,  106 => 16,  103 => 14,  100 => 13,  96 => 12,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Liste de mes Annonces{% endblock %}

{% block body %}


<br>
<div class=\"container\">
<div class=\"row\">

{% for annonce in annonces | reverse %}

{% if annonce.LeProducteur.id  %}
{% endif %}
{% if app.user.id == annonce.LeProducteur.id %}

<br>
<annonce>

        <div class=\"col-md3 mt-3 card mb-3 Tout {{annonce.laCategorie.libelle}}\" style=\"height:55rem\">
  <h3 class=\"card-header\">{{ annonce.titre }}</h3>
  <div class=\"card-body\">
    <h5 class=\"card-title\">Produit en vente:</h5>
    <h6 class=\"card-subtitle text-muted\">{{ annonce.libelleProduit }}</h6>
  </div>
<img src=\"https://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/73/736ada6647d9b5234d5e5e2662a3111bccf02f78_full.jpg\" eight=\"150px\" width=\"300\" alt=\"\">    <rect width=\"100%\" height=\"100%\" fill=\"#868e96\"></rect>
  </svg>
  <div class=\"card-body\">
    <p class=\"card-text\">Disponible du {{ annonce.creneauxDebut | date('d/m H:i') }} au {{ annonce.creneauxFin | date('d/m H:i') }}</p>
  </div>
  <ul class=\"list-group list-group-flush\">
    <li class=\"list-group-item\">Le prix est {{annonce.prixUnitaire}}</li>
    <li class=\"list-group-item\">Quantité disponible : {{annonce.qte}}</li>
    <li class=\"list-group-item\" style=\"width:350px\">Catégorie : {{annonce.laCategorie.libelle}}</li>
    
  </ul>
  <div class=\"card-body\">
    <a href=\"{{ path('annonce_show', {'id': annonce.id}) }}\" class=\"btn btn-primary\"> Lire la suite</a>
  </div>
</div>

</annonce>


{% endif %}
{% endfor %}

</div>
</div>

{% endblock %}", "matete/mesannonces.html.twig", "C:\\Users\\Matteo\\Matete\\templates\\matete\\mesannonces.html.twig");
    }
}
