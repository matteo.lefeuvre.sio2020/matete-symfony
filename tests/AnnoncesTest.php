<?php

namespace App\Tests;

use App\Entity\Annonce;
use PHPUnit\Framework\TestCase;

class AnnoncesTest extends TestCase
{
    public function testAssertTrue(): void
    {
        $annonce = new Annonce();

        $annonce->setTitre("mon titre");
        $annonce->setLibelleProduit("ma desc");
        $this->assertTrue($annonce->getTitre()==='mon titre',"Test du titre");
        $this->assertTrue($annonce->getLibelleProduit()==='ma desc',"Test du contenu");
    }

    public function testAssertFalse(): void
    {
        $annonce = new Annonce();

        $annonce->setTitre("mon titre");
        $annonce->setLibelleProduit("ma desc");
        $this->assertTrue($annonce->getTitre()==='mon titre',"Test du titre");
        $this->assertTrue($annonce->getLibelleProduit()==='ma desc',"Test du contenu");
    }

    public function testAssertEmpty(): void
    {
        $annonce = new Annonce();

        $annonce->setTitre("mon titre");
        $annonce->setLibelleProduit("ma desc");
        $this->assertTrue($annonce->getTitre()==='mon titre',"Test du titre");
        $this->assertTrue($annonce->getLibelleProduit()==='ma desc',"Test du contenu");
    }
}
