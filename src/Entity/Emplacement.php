<?php

namespace App\Entity;

use App\Repository\EmplacementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmplacementRepository::class)
 */
class Emplacement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descLieu;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\OneToMany(targetEntity=Annonce::class, mappedBy="lEmplacement")
     */
    private $lesAnnonces;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="lesEmplacements")
     */
    private $leProducteur;

    public function __construct()
    {
        $this->lesAnnonces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCooY(): ?string
    {
        return $this->cooY;
    }

    public function setCooY(string $cooY): self
    {
        $this->cooY = $cooY;

        return $this;
    }

    public function getCooX(): ?string
    {
        return $this->cooX;
    }

    public function setCooX(string $cooX): self
    {
        $this->cooX = $cooX;

        return $this;
    }

    public function getDescLieu(): ?string
    {
        return $this->descLieu;
    }

    public function setDescLieu(string $descLieu): self
    {
        $this->descLieu = $descLieu;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * @return Collection|Annonce[]
     */
    public function getLesAnnonces(): Collection
    {
        return $this->lesAnnonces;
    }

    public function addLesAnnonce(Annonce $lesAnnonce): self
    {
        if (!$this->lesAnnonces->contains($lesAnnonce)) {
            $this->lesAnnonces[] = $lesAnnonce;
            $lesAnnonce->setLEmplacement($this);
        }

        return $this;
    }

    public function removeLesAnnonce(Annonce $lesAnnonce): self
    {
        if ($this->lesAnnonces->removeElement($lesAnnonce)) {
            // set the owning side to null (unless already changed)
            if ($lesAnnonce->getLEmplacement() === $this) {
                $lesAnnonce->setLEmplacement(null);
            }
        }

        return $this;
    }

    public function getLeProducteur(): ?User
    {
        return $this->leProducteur;
    }

    public function setLeProducteur(?User $leProducteur): self
    {
        $this->leProducteur = $leProducteur;

        return $this;
    }

    public function __toString() {
        return $this->descLieu;
    }
}
