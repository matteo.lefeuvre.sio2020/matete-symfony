<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_4f829a8b7797e67e698b17e36d24f03d5b88591cf6d22659dc8244e14292a922 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 8
        echo "        <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootswatch@4.5.2/dist/lux/bootstrap.min.css\" integrity=\"sha384-9+PGKSqjRdkeAU7Eu4nkJU8RFaH8ace8HGXnkiKMP9I9Te0GJ4/km3L1Z8tXigpG\" crossorigin=\"anonymous\">
        ";
        // line 9
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 12
        echo "
        ";
        // line 13
        $this->displayBlock('javascripts', $context, $blocks);
        // line 16
        echo "        </head>
        <body>
            <nav class=\"navbar navbar-expand-lg navbar-dark bg-dark\">
        <div class=\"container-fluid\">
                    <a class=\"navbar-brand\" href=\"/\"><img src=\"https://cdn.discordapp.com/attachments/539117854247878686/912349805722877992/logo-removebg-preview.png\" width=\"60\" height=\"50\"> MATETE</a>
                    <button class=\"navbar-toggler\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#navbarColor01\" aria-controls=\"navbarColor01\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                    <span class=\"navbar-toggler-icon\"></span>
                    </button>

                    <div class=\"collapse navbar-collapse\" id=\"navbarColor01\">
                    <ul class=\"navbar-nav me-auto\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link \"  href=\"/annonces\">Annonces</a>
                        </li>
                        ";
        // line 30
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 30, $this->source); })()), "user", [], "any", false, false, false, 30)) {
            // line 31
            echo "                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/annonces/new\">Nouvelle annonce</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/mesannonces\">Mes Annonces</a>
                        </li> 
                        ";
        }
        // line 37
        echo "                                  
                    </ul>
                    <ul class=\"navbar-nav\" style=\"margin-left:auto\">
                    
                    ";
        // line 41
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 41, $this->source); })()), "user", [], "any", false, false, false, 41)) {
            // line 42
            echo "                        <li class=\"nav-item\">
                        <a class=\"btn btn-outline-dark\" href=\"";
            // line 43
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("panier");
            echo "\" > <svg xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\"
                            width=\"24\" height=\"24\"
                            viewBox=\"0 0 226 226\"
                            style=\" fill:#000000;\"><g fill=\"none\" fill-rule=\"nonzero\" stroke=\"none\" stroke-width=\"1\" stroke-linecap=\"butt\" stroke-linejoin=\"miter\" stroke-miterlimit=\"10\" stroke-dasharray=\"\" stroke-dashoffset=\"0\" font-family=\"none\" font-weight=\"none\" font-size=\"none\" text-anchor=\"none\" style=\"mix-blend-mode: normal\"><path d=\"M0,226v-226h226v226z\" fill=\"none\"></path><g fill=\"#ffffff\"><path d=\"M113,18.87931c-6.20773,0 -12.41831,2.34873 -17.11369,7.04411l-58.82658,58.82658h-15.96419c-7.25818,0 -12.94514,6.90813 -11.55013,14.03304l16.36881,83.65568c2.80118,14.32325 15.44256,24.72795 30.03401,24.72795h114.01156c14.59146,0 27.23284,-10.40469 30.03402,-24.72795l16.36882,-83.65568c1.39501,-7.12491 -4.29195,-14.03304 -11.55013,-14.03304h-15.87223l-58.83578,-58.82658c-4.69538,-4.69538 -10.89676,-7.04411 -17.10449,-7.04411zM113,32.91235c2.56389,0 5.12252,1.00272 7.11768,2.99788l48.84896,48.83976h-111.93327l48.83976,-48.83976c1.99516,-1.99516 4.56298,-2.99788 7.12687,-2.99788zM23.96468,98.875h14.98022c0.6437,0.08926 1.29665,0.08926 1.94035,0h144.24788c0.62859,0.08506 1.26578,0.08506 1.89437,0h14.92505l-15.81706,80.85091c-1.52107,7.77766 -8.25191,13.31576 -16.1757,13.31576h-114.01156c-7.92378,0 -14.65463,-5.53809 -16.1757,-13.31576zM58.66105,117.60718c-1.99464,0.05204 -3.87422,0.9453 -5.1742,2.45903c-1.29998,1.51372 -1.89907,3.50667 -1.6492,5.48629l4.70833,42.375c0.20694,2.56647 1.79405,4.81701 4.14211,5.87353c2.34806,1.05652 5.08501,0.75162 7.14298,-0.79575c2.05798,-1.54736 3.1109,-4.09201 2.74794,-6.6411l-4.70833,-42.375c-0.3563,-3.68955 -3.50411,-6.476 -7.20964,-6.382zM96.41048,117.60718c-3.8969,0.06088 -7.00805,3.26668 -6.95215,7.16365v42.375c-0.03602,2.54699 1.30215,4.91607 3.5021,6.20008c2.19995,1.28401 4.92084,1.28401 7.1208,0c2.19995,-1.28401 3.53812,-3.65309 3.5021,-6.20008v-42.375c0.0274,-1.90979 -0.71982,-3.74926 -2.07124,-5.09896c-1.35143,-1.3497 -3.19186,-2.09455 -5.10161,-2.0647zM129.36881,117.60718c-3.8969,0.06088 -7.00805,3.26668 -6.95215,7.16365v42.375c-0.03602,2.54699 1.30215,4.91607 3.5021,6.20008c2.19995,1.28401 4.92084,1.28401 7.1208,0c2.19995,-1.28401 3.53812,-3.65309 3.5021,-6.20008v-42.375c0.0274,-1.90979 -0.71982,-3.74926 -2.07124,-5.09896c-1.35143,-1.3497 -3.19186,-2.09455 -5.10161,-2.0647zM167.12744,117.60718c-3.62472,0.0165 -6.64858,2.77413 -6.99813,6.382l-4.70833,42.375c-0.36295,2.54909 0.68997,5.09374 2.74794,6.6411c2.05798,1.54736 4.79493,1.85227 7.14298,0.79575c2.34806,-1.05652 3.93517,-3.30706 4.14211,-5.87353l4.70833,-42.375c0.25375,-2.01582 -0.37284,-4.04328 -1.71969,-5.56443c-1.34685,-1.52115 -3.28351,-2.38865 -5.31522,-2.38088z\"></path></g></g></svg>
                        </a></li>
                        <li class=\"nav-item\"> 
                            <a class=\"btn btn-outline-info\" href=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profil", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 49, $this->source); })()), "user", [], "any", false, false, false, 49), "id", [], "any", false, false, false, 49)]), "html", null, true);
            echo "\">Profil</a> 
                        </li> 
                        <li class=\"nav-item\"> 
                            <a class=\"btn btn-outline-danger\" href=\"";
            // line 52
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
            echo "\">Déconnexion</a> 
                        </li> 
                    ";
        } else {
            // line 55
            echo "                        <li class=\"nav-item\">
                        <a class=\"btn btn-outline-dark\" href=\"";
            // line 56
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("panier");
            echo "\" > <svg xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\"
                            width=\"24\" height=\"24\"
                            viewBox=\"0 0 226 226\"
                            style=\" fill:#000000;\"><g fill=\"none\" fill-rule=\"nonzero\" stroke=\"none\" stroke-width=\"1\" stroke-linecap=\"butt\" stroke-linejoin=\"miter\" stroke-miterlimit=\"10\" stroke-dasharray=\"\" stroke-dashoffset=\"0\" font-family=\"none\" font-weight=\"none\" font-size=\"none\" text-anchor=\"none\" style=\"mix-blend-mode: normal\"><path d=\"M0,226v-226h226v226z\" fill=\"none\"></path><g fill=\"#ffffff\"><path d=\"M113,18.87931c-6.20773,0 -12.41831,2.34873 -17.11369,7.04411l-58.82658,58.82658h-15.96419c-7.25818,0 -12.94514,6.90813 -11.55013,14.03304l16.36881,83.65568c2.80118,14.32325 15.44256,24.72795 30.03401,24.72795h114.01156c14.59146,0 27.23284,-10.40469 30.03402,-24.72795l16.36882,-83.65568c1.39501,-7.12491 -4.29195,-14.03304 -11.55013,-14.03304h-15.87223l-58.83578,-58.82658c-4.69538,-4.69538 -10.89676,-7.04411 -17.10449,-7.04411zM113,32.91235c2.56389,0 5.12252,1.00272 7.11768,2.99788l48.84896,48.83976h-111.93327l48.83976,-48.83976c1.99516,-1.99516 4.56298,-2.99788 7.12687,-2.99788zM23.96468,98.875h14.98022c0.6437,0.08926 1.29665,0.08926 1.94035,0h144.24788c0.62859,0.08506 1.26578,0.08506 1.89437,0h14.92505l-15.81706,80.85091c-1.52107,7.77766 -8.25191,13.31576 -16.1757,13.31576h-114.01156c-7.92378,0 -14.65463,-5.53809 -16.1757,-13.31576zM58.66105,117.60718c-1.99464,0.05204 -3.87422,0.9453 -5.1742,2.45903c-1.29998,1.51372 -1.89907,3.50667 -1.6492,5.48629l4.70833,42.375c0.20694,2.56647 1.79405,4.81701 4.14211,5.87353c2.34806,1.05652 5.08501,0.75162 7.14298,-0.79575c2.05798,-1.54736 3.1109,-4.09201 2.74794,-6.6411l-4.70833,-42.375c-0.3563,-3.68955 -3.50411,-6.476 -7.20964,-6.382zM96.41048,117.60718c-3.8969,0.06088 -7.00805,3.26668 -6.95215,7.16365v42.375c-0.03602,2.54699 1.30215,4.91607 3.5021,6.20008c2.19995,1.28401 4.92084,1.28401 7.1208,0c2.19995,-1.28401 3.53812,-3.65309 3.5021,-6.20008v-42.375c0.0274,-1.90979 -0.71982,-3.74926 -2.07124,-5.09896c-1.35143,-1.3497 -3.19186,-2.09455 -5.10161,-2.0647zM129.36881,117.60718c-3.8969,0.06088 -7.00805,3.26668 -6.95215,7.16365v42.375c-0.03602,2.54699 1.30215,4.91607 3.5021,6.20008c2.19995,1.28401 4.92084,1.28401 7.1208,0c2.19995,-1.28401 3.53812,-3.65309 3.5021,-6.20008v-42.375c0.0274,-1.90979 -0.71982,-3.74926 -2.07124,-5.09896c-1.35143,-1.3497 -3.19186,-2.09455 -5.10161,-2.0647zM167.12744,117.60718c-3.62472,0.0165 -6.64858,2.77413 -6.99813,6.382l-4.70833,42.375c-0.36295,2.54909 0.68997,5.09374 2.74794,6.6411c2.05798,1.54736 4.79493,1.85227 7.14298,0.79575c2.34806,-1.05652 3.93517,-3.30706 4.14211,-5.87353l4.70833,-42.375c0.25375,-2.01582 -0.37284,-4.04328 -1.71969,-5.56443c-1.34685,-1.52115 -3.28351,-2.38865 -5.31522,-2.38088z\"></path></g></g></svg>
                        </a></li> 
                        <li class=\"nav-item\">
                            <a class=\"btn btn-outline-success\" href=\"/inscription\">Inscription</a>
                        </li>  
                        <li class=\"nav-item\"> 
                            <a class=\"btn btn-outline-info\" href=\"";
            // line 65
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_login");
            echo "\">Connexion</a> 
                        </li> 
                    ";
        }
        // line 67
        echo " 
                    </ul>   
                </div>
            </nav>
            <div class=\"container\">
                ";
        // line 72
        $this->displayBlock('body', $context, $blocks);
        // line 74
        echo "           
            </div>
            <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p\" crossorigin=\"anonymous\"></script>
            
    </body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 10
        echo "            ";
        // line 11
        echo "        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 13
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 14
        echo "            ";
        // line 15
        echo "        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 72
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 73
        echo "                
                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 73,  235 => 72,  225 => 15,  223 => 14,  213 => 13,  203 => 11,  201 => 10,  191 => 9,  172 => 5,  156 => 74,  154 => 72,  147 => 67,  141 => 65,  129 => 56,  126 => 55,  120 => 52,  114 => 49,  105 => 43,  102 => 42,  100 => 41,  94 => 37,  85 => 31,  83 => 30,  67 => 16,  65 => 13,  62 => 12,  60 => 9,  57 => 8,  53 => 5,  47 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>{% block title %}Welcome!{% endblock %}</title>
        {# Run `composer require symfony/webpack-encore-bundle`
           and uncomment the following Encore helpers to start using Symfony UX #}
        <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootswatch@4.5.2/dist/lux/bootstrap.min.css\" integrity=\"sha384-9+PGKSqjRdkeAU7Eu4nkJU8RFaH8ace8HGXnkiKMP9I9Te0GJ4/km3L1Z8tXigpG\" crossorigin=\"anonymous\">
        {% block stylesheets %}
            {#{{ encore_entry_link_tags('app') }}#}
        {% endblock %}

        {% block javascripts %}
            {#{{ encore_entry_script_tags('app') }}#}
        {% endblock %}
        </head>
        <body>
            <nav class=\"navbar navbar-expand-lg navbar-dark bg-dark\">
        <div class=\"container-fluid\">
                    <a class=\"navbar-brand\" href=\"/\"><img src=\"https://cdn.discordapp.com/attachments/539117854247878686/912349805722877992/logo-removebg-preview.png\" width=\"60\" height=\"50\"> MATETE</a>
                    <button class=\"navbar-toggler\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#navbarColor01\" aria-controls=\"navbarColor01\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                    <span class=\"navbar-toggler-icon\"></span>
                    </button>

                    <div class=\"collapse navbar-collapse\" id=\"navbarColor01\">
                    <ul class=\"navbar-nav me-auto\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link \"  href=\"/annonces\">Annonces</a>
                        </li>
                        {% if app.user %}
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/annonces/new\">Nouvelle annonce</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/mesannonces\">Mes Annonces</a>
                        </li> 
                        {% endif %}                                  
                    </ul>
                    <ul class=\"navbar-nav\" style=\"margin-left:auto\">
                    
                    {% if app.user %}
                        <li class=\"nav-item\">
                        <a class=\"btn btn-outline-dark\" href=\"{{ path('panier')}}\" > <svg xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\"
                            width=\"24\" height=\"24\"
                            viewBox=\"0 0 226 226\"
                            style=\" fill:#000000;\"><g fill=\"none\" fill-rule=\"nonzero\" stroke=\"none\" stroke-width=\"1\" stroke-linecap=\"butt\" stroke-linejoin=\"miter\" stroke-miterlimit=\"10\" stroke-dasharray=\"\" stroke-dashoffset=\"0\" font-family=\"none\" font-weight=\"none\" font-size=\"none\" text-anchor=\"none\" style=\"mix-blend-mode: normal\"><path d=\"M0,226v-226h226v226z\" fill=\"none\"></path><g fill=\"#ffffff\"><path d=\"M113,18.87931c-6.20773,0 -12.41831,2.34873 -17.11369,7.04411l-58.82658,58.82658h-15.96419c-7.25818,0 -12.94514,6.90813 -11.55013,14.03304l16.36881,83.65568c2.80118,14.32325 15.44256,24.72795 30.03401,24.72795h114.01156c14.59146,0 27.23284,-10.40469 30.03402,-24.72795l16.36882,-83.65568c1.39501,-7.12491 -4.29195,-14.03304 -11.55013,-14.03304h-15.87223l-58.83578,-58.82658c-4.69538,-4.69538 -10.89676,-7.04411 -17.10449,-7.04411zM113,32.91235c2.56389,0 5.12252,1.00272 7.11768,2.99788l48.84896,48.83976h-111.93327l48.83976,-48.83976c1.99516,-1.99516 4.56298,-2.99788 7.12687,-2.99788zM23.96468,98.875h14.98022c0.6437,0.08926 1.29665,0.08926 1.94035,0h144.24788c0.62859,0.08506 1.26578,0.08506 1.89437,0h14.92505l-15.81706,80.85091c-1.52107,7.77766 -8.25191,13.31576 -16.1757,13.31576h-114.01156c-7.92378,0 -14.65463,-5.53809 -16.1757,-13.31576zM58.66105,117.60718c-1.99464,0.05204 -3.87422,0.9453 -5.1742,2.45903c-1.29998,1.51372 -1.89907,3.50667 -1.6492,5.48629l4.70833,42.375c0.20694,2.56647 1.79405,4.81701 4.14211,5.87353c2.34806,1.05652 5.08501,0.75162 7.14298,-0.79575c2.05798,-1.54736 3.1109,-4.09201 2.74794,-6.6411l-4.70833,-42.375c-0.3563,-3.68955 -3.50411,-6.476 -7.20964,-6.382zM96.41048,117.60718c-3.8969,0.06088 -7.00805,3.26668 -6.95215,7.16365v42.375c-0.03602,2.54699 1.30215,4.91607 3.5021,6.20008c2.19995,1.28401 4.92084,1.28401 7.1208,0c2.19995,-1.28401 3.53812,-3.65309 3.5021,-6.20008v-42.375c0.0274,-1.90979 -0.71982,-3.74926 -2.07124,-5.09896c-1.35143,-1.3497 -3.19186,-2.09455 -5.10161,-2.0647zM129.36881,117.60718c-3.8969,0.06088 -7.00805,3.26668 -6.95215,7.16365v42.375c-0.03602,2.54699 1.30215,4.91607 3.5021,6.20008c2.19995,1.28401 4.92084,1.28401 7.1208,0c2.19995,-1.28401 3.53812,-3.65309 3.5021,-6.20008v-42.375c0.0274,-1.90979 -0.71982,-3.74926 -2.07124,-5.09896c-1.35143,-1.3497 -3.19186,-2.09455 -5.10161,-2.0647zM167.12744,117.60718c-3.62472,0.0165 -6.64858,2.77413 -6.99813,6.382l-4.70833,42.375c-0.36295,2.54909 0.68997,5.09374 2.74794,6.6411c2.05798,1.54736 4.79493,1.85227 7.14298,0.79575c2.34806,-1.05652 3.93517,-3.30706 4.14211,-5.87353l4.70833,-42.375c0.25375,-2.01582 -0.37284,-4.04328 -1.71969,-5.56443c-1.34685,-1.52115 -3.28351,-2.38865 -5.31522,-2.38088z\"></path></g></g></svg>
                        </a></li>
                        <li class=\"nav-item\"> 
                            <a class=\"btn btn-outline-info\" href=\"{{ path('profil', {'id': app.user.id})  }}\">Profil</a> 
                        </li> 
                        <li class=\"nav-item\"> 
                            <a class=\"btn btn-outline-danger\" href=\"{{ path('app_logout') }}\">Déconnexion</a> 
                        </li> 
                    {% else %}
                        <li class=\"nav-item\">
                        <a class=\"btn btn-outline-dark\" href=\"{{ path('panier')}}\" > <svg xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\"
                            width=\"24\" height=\"24\"
                            viewBox=\"0 0 226 226\"
                            style=\" fill:#000000;\"><g fill=\"none\" fill-rule=\"nonzero\" stroke=\"none\" stroke-width=\"1\" stroke-linecap=\"butt\" stroke-linejoin=\"miter\" stroke-miterlimit=\"10\" stroke-dasharray=\"\" stroke-dashoffset=\"0\" font-family=\"none\" font-weight=\"none\" font-size=\"none\" text-anchor=\"none\" style=\"mix-blend-mode: normal\"><path d=\"M0,226v-226h226v226z\" fill=\"none\"></path><g fill=\"#ffffff\"><path d=\"M113,18.87931c-6.20773,0 -12.41831,2.34873 -17.11369,7.04411l-58.82658,58.82658h-15.96419c-7.25818,0 -12.94514,6.90813 -11.55013,14.03304l16.36881,83.65568c2.80118,14.32325 15.44256,24.72795 30.03401,24.72795h114.01156c14.59146,0 27.23284,-10.40469 30.03402,-24.72795l16.36882,-83.65568c1.39501,-7.12491 -4.29195,-14.03304 -11.55013,-14.03304h-15.87223l-58.83578,-58.82658c-4.69538,-4.69538 -10.89676,-7.04411 -17.10449,-7.04411zM113,32.91235c2.56389,0 5.12252,1.00272 7.11768,2.99788l48.84896,48.83976h-111.93327l48.83976,-48.83976c1.99516,-1.99516 4.56298,-2.99788 7.12687,-2.99788zM23.96468,98.875h14.98022c0.6437,0.08926 1.29665,0.08926 1.94035,0h144.24788c0.62859,0.08506 1.26578,0.08506 1.89437,0h14.92505l-15.81706,80.85091c-1.52107,7.77766 -8.25191,13.31576 -16.1757,13.31576h-114.01156c-7.92378,0 -14.65463,-5.53809 -16.1757,-13.31576zM58.66105,117.60718c-1.99464,0.05204 -3.87422,0.9453 -5.1742,2.45903c-1.29998,1.51372 -1.89907,3.50667 -1.6492,5.48629l4.70833,42.375c0.20694,2.56647 1.79405,4.81701 4.14211,5.87353c2.34806,1.05652 5.08501,0.75162 7.14298,-0.79575c2.05798,-1.54736 3.1109,-4.09201 2.74794,-6.6411l-4.70833,-42.375c-0.3563,-3.68955 -3.50411,-6.476 -7.20964,-6.382zM96.41048,117.60718c-3.8969,0.06088 -7.00805,3.26668 -6.95215,7.16365v42.375c-0.03602,2.54699 1.30215,4.91607 3.5021,6.20008c2.19995,1.28401 4.92084,1.28401 7.1208,0c2.19995,-1.28401 3.53812,-3.65309 3.5021,-6.20008v-42.375c0.0274,-1.90979 -0.71982,-3.74926 -2.07124,-5.09896c-1.35143,-1.3497 -3.19186,-2.09455 -5.10161,-2.0647zM129.36881,117.60718c-3.8969,0.06088 -7.00805,3.26668 -6.95215,7.16365v42.375c-0.03602,2.54699 1.30215,4.91607 3.5021,6.20008c2.19995,1.28401 4.92084,1.28401 7.1208,0c2.19995,-1.28401 3.53812,-3.65309 3.5021,-6.20008v-42.375c0.0274,-1.90979 -0.71982,-3.74926 -2.07124,-5.09896c-1.35143,-1.3497 -3.19186,-2.09455 -5.10161,-2.0647zM167.12744,117.60718c-3.62472,0.0165 -6.64858,2.77413 -6.99813,6.382l-4.70833,42.375c-0.36295,2.54909 0.68997,5.09374 2.74794,6.6411c2.05798,1.54736 4.79493,1.85227 7.14298,0.79575c2.34806,-1.05652 3.93517,-3.30706 4.14211,-5.87353l4.70833,-42.375c0.25375,-2.01582 -0.37284,-4.04328 -1.71969,-5.56443c-1.34685,-1.52115 -3.28351,-2.38865 -5.31522,-2.38088z\"></path></g></g></svg>
                        </a></li> 
                        <li class=\"nav-item\">
                            <a class=\"btn btn-outline-success\" href=\"/inscription\">Inscription</a>
                        </li>  
                        <li class=\"nav-item\"> 
                            <a class=\"btn btn-outline-info\" href=\"{{ path('app_login') }}\">Connexion</a> 
                        </li> 
                    {% endif %} 
                    </ul>   
                </div>
            </nav>
            <div class=\"container\">
                {% block body %}
                
                {% endblock %}           
            </div>
            <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p\" crossorigin=\"anonymous\"></script>
            
    </body>
</html>
", "base.html.twig", "C:\\Users\\Matteo\\Matete\\templates\\base.html.twig");
    }
}
