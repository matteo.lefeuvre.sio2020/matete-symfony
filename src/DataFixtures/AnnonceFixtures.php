<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Categorie;
use App\Entity\Emplacement;
use App\Entity\Annonce;
use Faker;

class AnnonceFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker= \Faker\Factory::create("fr_FR");

        for($i = 0; $i < 10; $i++) {
        
        $Producteur = new User();
        $Producteur->setNom($faker->lastName);
        $Producteur->setPrenom($faker->firstName);
        $Producteur->setTel($faker->phoneNumber);
        $Producteur->setEmail($faker->freeEmail);
        $Producteur->setPassword($faker->password);
        $manager->persist($Producteur);

        $Categorie = new Categorie();
        $Categorie->setLibelle($faker->text($maxNbChars = 200));
        $manager->persist($Categorie);

        $Emplacement = new Emplacement();
        $Emplacement->setCooY($faker->latitude);
        $Emplacement->setCooX($faker->longitude);
        $Emplacement->setDescLieu($faker->text($maxNbChars = 200));
        $Emplacement->setNom($faker->text($maxNbChars = 20));
        $Emplacement->setAdresse($faker->address);
        $Emplacement->setLeProducteur($Producteur);
        $manager->persist($Emplacement);

        $Annonce = new Annonce();
        $Annonce->setTitre($faker->text($maxNbChars =20))
                ->setCreneauxDebut($faker->DateTime)
                ->setCreneauxFin($faker->DateTime)
                ->setLibelleProduit($faker->text($maxNbChars =20))
                ->setPrixUnitaire($faker->randomNumber($nbDigits = NULL, $strict = false))
                ->setQte($faker->randomNumber)
                ->setLeProducteur($Producteur)
                ->setLaCategorie($Categorie)
                ->setLEmplacement($Emplacement);
        $manager->persist($Annonce);



        $manager->flush();
        }
    }
}
