<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Categorie;

class CategorieFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $categorie = new Categorie();
        $categorie->setLibelle("Fruit");
        $manager->persist($categorie);
        $categorie2 = new Categorie();
        $categorie2->setLibelle("Legume");
        $manager->persist($categorie2);
        $manager->flush();
    }
}
