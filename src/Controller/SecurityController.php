<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\RegistrationType;
use App\Entity\User;
use App\Entity\Annonce;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Repository\AnnonceRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class SecurityController extends AbstractController
{

    private $security;

    public function __construct(security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/security", name="security")
     */
    public function index(): Response
    {
        return $this->render('security/index.html.twig', [
            'controller_name' => 'SecurityController',
        ]);
    }

    /**
     * @Route("/inscription", name="security_registration")
     */
    public function registration(Request $request, EntityManagerInterface $manager, UserPasswordHasherInterface $encoder){
        $user = new User();

        $form = $this->createForm(RegistrationType::class,$user);
        //récupérer réponse
        $form->handleRequest($request);
        //si formulaire valide, on persiste en bdd
        if($form->isSubmitted()&&$form->isValid()){
            $plaintextpassword = $user->getPassword();

            $hashedPassword = $encoder->hashPassword(
                $user,
                $plaintextpassword
            );
            $user->setPassword($hashedPassword)
                 ->addRole("ROLE_USER");


            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute("confirm") ;           
        }

        return $this->render('security/registration.html.twig',[
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): void
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    } 

    /**
     * @Route("/profil/{id}", name="profil")
    */
    public function profil()
    {

        return $this->render('security/profil.html.twig', [
        ]);
    }

    /**
     * @Route("/profil/{id}/edit", name="profil_edit")
    */ 
    public function editProfil(Request $request, UserRepository $vendeur, $id , EntityManagerInterface $manager): Response
    {

        $user = $this->security->getUser();
        $vendeur = $vendeur->find($id);


        $form = $this->createForm(UserType::class,$vendeur);
        //récupérer réponse
        $form->handleRequest($request);
        //si formulaire valide, on persiste en bdd
        
        if($form->isSubmitted()){


            $manager->persist($vendeur);
            $manager->flush();

            return $this->redirectToRoute('profil', ['id'=>$vendeur->getId()
            ]);
        }
        else{
            echo "<script>console.log( 'error' );</script>";
        }

        return $this->render('security/profil_edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete_user" , name="user_delete")
     */
    public function deleteUser(EntityManagerInterface $repo){
        
        $user = $this->security->getUser();
        $session = $this->get('session');
        $session = new Session();
        $session->invalidate();
        $lesAnnonces = $user->lesAnnonces;
        if(count($lesAnnonces) > 0){
            for ($i=0; $i < count(lesAnnonces); $i++) { 
                $del = $this->getDoctrine()->getManager();
                $del->remove($lesAnnonces[i]);
                $del->flush();
            }
        }
        $repo->remove($user);
        $repo->flush();

        return $this->redirectToRoute("matete");

    }

    /**
     * @Route("/confirm" , name="confirm")
     */
    public function confirm(){


        return $this->render('security/confirm.html.twig', [
        ]);
    }
}


