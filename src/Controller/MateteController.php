<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Annonce;
use App\Entity\Categorie;
use App\Entity\Emplacement;
use App\Repository\AnnonceRepository;
use App\Repository\CategorieRepository;
use App\Repository\EmplacementRepository; 
use App\Form\AnnonceType;
use App\Form\CategorieType;
use App\Form\EmplacementType;
use Symfony\Bundle\FrameworkBundle\DependencyInjection\Compiler\SessionPass;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Security;


class MateteController extends AbstractController
{
    private $security;

    public function __construct(security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/", name="matete")
     */
    public function index(): Response
    {
        return $this->render('matete/home.html.twig', [
            'controller_name' => 'MateteController',
        ]);
    }

    /**
     * @Route("/matete", name="home")
     */
    public function home(){
        return $this->render('matete/index.html.twig', [
            
        ]);
    }

    /**
     * @Route("/annonces", name="annonces")
     */
    public function annonces(AnnonceRepository $Annrepo, CategorieRepository $Catrepo){
        
        $annonces = $Annrepo->findAll();
        $categories = $Catrepo->findAll();

        return $this->render('matete/annonces.html.twig', [
            'annonces' => $annonces,
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/annonce/{id}", name="annonce_show")
     */
    public function show(annonce $annonce){

        return $this->render('matete/show.html.twig', [
            'annonce' => $annonce
        ]);
    }

    /**
     * @Route("/annonces/new", name="new")
     * @Route("/annonce/{id}/edit", name="annonce_edit")
     */
    public function creer(Annonce $annonce = null , Request $request, EntityManagerInterface $manager){

        if(!$annonce) {
            $annonce = new Annonce();
        } 


        
        $user = $this->security->getUser();

        $form = $this->createFormBuilder($annonce)
        ->add('titre')
        ->add('creneauxDebut')
        ->add('creneauxFin')
        ->add('libelleProduit')
        ->add('prixUnitaire')
        ->add('qte')
        ->add('laCategorie')
        ->add('lEmplacement', EmplacementType::class) 
        ->add('Image')
        ->getForm()
        ;

        if($form->isSubmitted() && $form->isValid()) {
            if(!$annonce->getId()){

            }
            $annonce->setLeProducteur($user);
            $manager->persist($annonce);
            $manager->flush();

            return $this->redirectToRoute('annonce_show', ['id' => $annonce->getId
            ()]);
        }

        if( $annonce->getLeProducteur()->getId() != $user->getId() ){

            return $this->render('security/impossible.html.twig');
        }else{

        return $this->render('matete/creer.html.twig', [
            'formAnnonce' => $form->createView(),
            'editMode' => $annonce->getId() !== null,
            'annonce' => $annonce
        ]);
    }
    }

    /**
     * @Route("/categorie/new", name="ajout_categorie")
     */
    public function newCategorie(Categorie $categorie = null , Request $request, EntityManagerInterface $manager)
    {

        $categorie = new Categorie();
        
        $form = $this->createForm(CategorieType::class, $categorie);
        

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            if(!$categorie->getId()){

            }
            $manager->persist($categorie);
            $manager->flush();
    
            return $this->redirectToRoute('annonces');
        }

    return $this->render('matete/newCategorie.html.twig', [  
        'formCategorie' => $form->createView(),        
    ]);
    }

    /**
     * @Route("/mesannonces", name="mesannonces")
     */
    public function mesannonces(AnnonceRepository $repo){
        
        $annonces = $repo->findAll();

        return $this->render('matete/mesannonces.html.twig', [
            'annonces' => $annonces
        ]);
    }

    /**
     * @Route("/amongus", name="amongus")
     */
    public function amongus()
    {

        return $this->render('matete/amongus.html.twig', [
        ]);
    }
    
    /**
     * @Route("/panier", name="panier")
     * @param SessionInterface $session
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function panier(AnnonceRepository $repo, SessionInterface $session){

        $session->has('panier')?$panier = $session->get('panier'):$panier = array();

        $annonces = $repo->findBy(["id"=>$panier]);

        return $this->render('matete/panier.html.twig', ['annonces' => $annonces
        ]);
    }

    /**
     * @Route("/ajoutpanier/{id}", name="ajoutpanier")
     * @param $titre
     * @param SessionInterface
     * @param string
     */
    public function ajoutpanier($id, SessionInterface $session): RedirectResponse
    {
        
        $session->has('panier')?$panier = $session->get('panier'):$panier = array();

        $panier[$id]= $id;

        $session->set('panier' , $panier);
        
        
        return $this->redirectToRoute('annonces');
    }

    /**
     * @Route("/retirerpanier/{id}", name="retirerpanier")
     * @param $titre
     * @param SessionInterface
     * @param string
     */
    public function retirerpanier($id, SessionInterface $session): RedirectResponse
{
        
        $panier = $session->get('panier', []);

         if(!empty($panier[$id])){
             unset($panier[$id]);
         }

        $session->set('panier', $panier);
        
        return $this->redirectToRoute('panier');
    }

    /**
    * @Route("/matete/{id}/delete", name="matete_delete")
    */

    public function delete(Annonce $annonce){
        
        $del = $this->getDoctrine()->getManager();
        $del->remove($annonce);
        $del->flush();

        return $this->redirectToRoute("annonces");

    }

    /**
     * @Route("/delete/{id}" , name="annonce_delete")
     */
    public function deleteAction(EntityManagerInterface $entityManager, AnnonceRepository $annonce, $id) {
        $annonce = $annonce->find($id);
        if (!$annonce) {
            throw $this->createNotFoundException(
                "Il n'y a pas d'annonce ayant l'id : " . $id
            );
        }
        $entityManager->remove($annonce);
        $entityManager->flush();

        return $this->redirectToRoute('lesAnnonces');
    }

    /**
     * @Route("/map" , name="map")
     */
    public function map() {

        return $this->render('matete/map.html.twig' );
    }

    /**
     * @Route("/mesemplacement", name="mesemplacement")
     */
    public function mesemplacement(EmplacementRepository $repo){
        
        $emplacement = $repo->findAll();

        return $this->render('matete/mesemplacement.html.twig', [
            'emplacements' => $emplacement
        ]);
    }

    /**
     * @Route("/emplacement/new", name="ajout_emplacement")
     */
    public function newEmplacement(Emplacement $emplacement = null , Request $request, EntityManagerInterface $manager)
    {

        $emplacement = new Emplacement();
        $user = $this->security->getUser();

        $form = $this->createForm(EmplacementType::class, $emplacement);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            if(!$emplacement->getId()){

            }
            $emplacement->setLeProducteur($user);
            $manager->persist($emplacement);
            $manager->flush();
    
            return $this->redirectToRoute('annonces');
        }

    return $this->render('matete/newEmplacement.html.twig', [  
        'formEmplacement' => $form->createView(),        
    ]);
    }

    /**
     * @Route("/delete/{id}" , name="annonce_delete")
     */
    public function deleteEmplacement(EntityManagerInterface $entityManager, AnnonceRepository $annonce, $id) {
        $annonce = $annonce->find($id);
        if (!$annonce) {
            throw $this->createNotFoundException(
                "Il n'y a pas d'annonce ayant l'id : " . $id
            );
        }
        $entityManager->remove($annonce);
        $entityManager->flush();

        return $this->redirectToRoute('lesAnnonces');
    }
    

    }


    
    



