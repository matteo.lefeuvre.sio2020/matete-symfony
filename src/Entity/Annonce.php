<?php

namespace App\Entity;

use App\Repository\AnnonceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AnnonceRepository::class)
 */
class Annonce
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=10, max=255, minMessage="Le titre est trop court", maxMessage="Le titre est trop long")
     */
    private $titre;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creneauxDebut;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creneauxFin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelleProduit;

    /**
     * @ORM\Column(type="float")
     */
    private $prixUnitaire;

    /**
     * @ORM\Column(type="integer")
     */
    private $qte;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="lesAnnonces")
     * @ORM\JoinColumn(nullable=false)
     */
    private $leProducteur;

    /**
     * @ORM\ManyToOne(targetEntity=Categorie::class, inversedBy="lesAnnonces")
     * @ORM\JoinColumn(nullable=false)
     */
    private $laCategorie;

    /**
     * @ORM\ManyToOne(targetEntity=Emplacement::class, inversedBy="lesAnnonces")
     */
    private $lEmplacement;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Image;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getCreneauxDebut(): ?\DateTimeInterface
    {
        return $this->creneauxDebut;
    }

    public function setCreneauxDebut(\DateTimeInterface $creneauxDebut): self
    {
        $this->creneauxDebut = $creneauxDebut;

        return $this;
    }

    public function getCreneauxFin(): ?\DateTimeInterface
    {
        return $this->creneauxFin;
    }

    public function setCreneauxFin(\DateTimeInterface $creneauxFin): self
    {
        $this->creneauxFin = $creneauxFin;

        return $this;
    }

    public function getLibelleProduit(): ?string
    {
        return $this->libelleProduit;
    }

    public function setLibelleProduit(string $libelleProduit): self
    {
        $this->libelleProduit = $libelleProduit;

        return $this;
    }

    public function getPrixUnitaire(): ?float
    {
        return $this->prixUnitaire;
    }

    public function setPrixUnitaire(float $prixUnitaire): self
    {
        $this->prixUnitaire = $prixUnitaire;

        return $this;
    }

    public function getQte(): ?int
    {
        return $this->qte;
    }

    public function setQte(int $qte): self
    {
        $this->qte = $qte;

        return $this;
    }

    public function getLeProducteur(): ?User
    {
        return $this->leProducteur;
    }

    public function setLeProducteur(?User $leProducteur): self
    {
        $this->leProducteur = $leProducteur;

        return $this;
    }

    public function getLaCategorie(): ?Categorie
    {
        return $this->laCategorie;
    }

    public function setLaCategorie(?Categorie $laCategorie): self
    {
        $this->laCategorie = $laCategorie;

        return $this;
    }

    public function getLEmplacement(): ?Emplacement
    {
        return $this->lEmplacement;
    }

    public function setLEmplacement(?Emplacement $lEmplacement): self
    {
        $this->lEmplacement = $lEmplacement;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->Image;
    }

    public function setImage(string $Image): self
    {
        $this->Image = $Image;

        return $this;
    }
}
