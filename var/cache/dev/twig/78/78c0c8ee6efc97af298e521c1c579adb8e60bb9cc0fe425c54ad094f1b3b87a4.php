<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* matete/annonces.html.twig */
class __TwigTemplate_96ee7943ee2aa307f229f6dc3b110e843c500285ec35ab47bed9e132793dbb7b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/annonces.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "matete/annonces.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "matete/annonces.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Liste des Annonces";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
<br>
<div>
      <select class=\"form-select\" style=\"width:20%\"onchange=\"changeCategorie(this);\">
        <option value='Tout'>Tout</option>
        ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) || array_key_exists("categories", $context) ? $context["categories"] : (function () { throw new RuntimeError('Variable "categories" does not exist.', 11, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 12
            echo "            <option value=";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "libelle", [], "any", false, false, false, 12), "html", null, true);
            echo ">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "libelle", [], "any", false, false, false, 12), "html", null, true);
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "      </select>
</div>

<br>
<div class=\"container\">
<div class=\"row\">

";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_reverse_filter($this->env, (isset($context["annonces"]) || array_key_exists("annonces", $context) ? $context["annonces"] : (function () { throw new RuntimeError('Variable "annonces" does not exist.', 21, $this->source); })())));
        foreach ($context['_seq'] as $context["_key"] => $context["annonce"]) {
            // line 22
            echo "
<annonce>

        <div class=\"col-md3 mt-3 card mb-3 Tout ";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["annonce"], "laCategorie", [], "any", false, false, false, 25), "libelle", [], "any", false, false, false, 25), "html", null, true);
            echo "\" style=\"height:55rem\">
  <h3 class=\"card-header\">";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "titre", [], "any", false, false, false, 26), "html", null, true);
            echo "</h3>
  <div class=\"card-body\">
    <h5 class=\"card-title\">Produit en vente:</h5>
    <h6 class=\"card-subtitle text-muted\">";
            // line 29
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "libelleProduit", [], "any", false, false, false, 29), "html", null, true);
            echo "</h6>
  </div>
<img src=";
            // line 31
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "Image", [], "any", false, false, false, 31), "html", null, true);
            echo " eight=\"200px\" width=\"375\">
  <div class=\"card-body\">
    <p class=\"card-text\">Disponible du ";
            // line 33
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "creneauxDebut", [], "any", false, false, false, 33), "d/m H:i"), "html", null, true);
            echo " au ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "creneauxFin", [], "any", false, false, false, 33), "d/m H:i"), "html", null, true);
            echo "</p>
  </div>
  <ul class=\"list-group list-group-flush\">
    <li class=\"list-group-item\">Le prix est ";
            // line 36
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "prixUnitaire", [], "any", false, false, false, 36), "html", null, true);
            echo "</li>
    <li class=\"list-group-item\">Quantité disponible : ";
            // line 37
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["annonce"], "qte", [], "any", false, false, false, 37), "html", null, true);
            echo "</li>
    <li class=\"list-group-item\" style=\"width:350px\">Catégorie : ";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["annonce"], "laCategorie", [], "any", false, false, false, 38), "libelle", [], "any", false, false, false, 38), "html", null, true);
            echo "</li>
    
  </ul>
  <div class=\"card-body\">
    <a href=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("annonce_show", ["id" => twig_get_attribute($this->env, $this->source, $context["annonce"], "id", [], "any", false, false, false, 42)]), "html", null, true);
            echo "\" class=\"btn btn-primary\"> Lire la suite</a>
  </div>
</div>

</annonce>
<br>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['annonce'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "
</div>
</div>


<script>
function changeCategorie(selectedOption){
    var value = selectedOption.value;

    //récupère toutes les cards
    var allCards = document.getElementsByClassName(\"Tout\");
    //on fait d'abord disparaitre toutes les cards sans exception
    for(var i = 0; i < allCards.length; i++){
        allCards[i].style.display = \"none\";
    }

    //récupère les cards ayant pour categorie la categorie selectionnée avec le select
    var relatedCards = document.getElementsByClassName(value);
    //re-affiche les annonces ayant la categorie choisie dans le select
    for(var i = 0; i < relatedCards.length; i++){
        relatedCards[i].style.display = \"block\";
    }
}
</script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "matete/annonces.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 49,  171 => 42,  164 => 38,  160 => 37,  156 => 36,  148 => 33,  143 => 31,  138 => 29,  132 => 26,  128 => 25,  123 => 22,  119 => 21,  110 => 14,  99 => 12,  95 => 11,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Liste des Annonces{% endblock %}

{% block body %}

<br>
<div>
      <select class=\"form-select\" style=\"width:20%\"onchange=\"changeCategorie(this);\">
        <option value='Tout'>Tout</option>
        {% for category in categories %}
            <option value={{category.libelle}}>{{category.libelle}}</option>
        {% endfor %}
      </select>
</div>

<br>
<div class=\"container\">
<div class=\"row\">

{% for annonce in annonces | reverse %}

<annonce>

        <div class=\"col-md3 mt-3 card mb-3 Tout {{annonce.laCategorie.libelle}}\" style=\"height:55rem\">
  <h3 class=\"card-header\">{{ annonce.titre }}</h3>
  <div class=\"card-body\">
    <h5 class=\"card-title\">Produit en vente:</h5>
    <h6 class=\"card-subtitle text-muted\">{{ annonce.libelleProduit }}</h6>
  </div>
<img src={{ annonce.Image }} eight=\"200px\" width=\"375\">
  <div class=\"card-body\">
    <p class=\"card-text\">Disponible du {{ annonce.creneauxDebut | date('d/m H:i') }} au {{ annonce.creneauxFin | date('d/m H:i') }}</p>
  </div>
  <ul class=\"list-group list-group-flush\">
    <li class=\"list-group-item\">Le prix est {{annonce.prixUnitaire}}</li>
    <li class=\"list-group-item\">Quantité disponible : {{annonce.qte}}</li>
    <li class=\"list-group-item\" style=\"width:350px\">Catégorie : {{annonce.laCategorie.libelle}}</li>
    
  </ul>
  <div class=\"card-body\">
    <a href=\"{{ path('annonce_show', {'id': annonce.id}) }}\" class=\"btn btn-primary\"> Lire la suite</a>
  </div>
</div>

</annonce>
<br>
{% endfor %}

</div>
</div>


<script>
function changeCategorie(selectedOption){
    var value = selectedOption.value;

    //récupère toutes les cards
    var allCards = document.getElementsByClassName(\"Tout\");
    //on fait d'abord disparaitre toutes les cards sans exception
    for(var i = 0; i < allCards.length; i++){
        allCards[i].style.display = \"none\";
    }

    //récupère les cards ayant pour categorie la categorie selectionnée avec le select
    var relatedCards = document.getElementsByClassName(value);
    //re-affiche les annonces ayant la categorie choisie dans le select
    for(var i = 0; i < relatedCards.length; i++){
        relatedCards[i].style.display = \"block\";
    }
}
</script>

{% endblock %}", "matete/annonces.html.twig", "C:\\Users\\Matteo\\Matete\\templates\\matete\\annonces.html.twig");
    }
}
